package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

/**
 * The Inventory class
 * @author CS1302
 * @version 1.0
 *
 */
public class Inventory {
	private String name;
	private ArrayList<Automobile> automobiles;
	
	/**
	 * Initializes a new Inventory
	 * 
	 * @precondition none
	 * @postcondition getAutomobiles().size() == 0
	 * 
	 * 
	 */
	public Inventory() {
		this.automobiles = new ArrayList<Automobile>();
	}
	
	/** Initializes a new Inventory
	 * 
	 * @precondition name != null
	 * @postcondition getName() == name 
	 * 
	 * @param name name of the dealership
	 */
	public Inventory(String name) {
		if (name == null) {
			throw new IllegalArgumentException("name should not be null");
		}
		this.name = name;
	}
	
	/**
	 * Gets the collection of automobiles
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the collection of automobiles
	 */
	public ArrayList<Automobile> getAutomobiles() {
		return this.automobiles;
	}
	
	/** gets the name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	/** sets the name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param newName sets the new name
	 */
	
	public void setName(String newName) {
		this.name = newName;
    }
	/** returns the number of automobiles in the collection
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return number of automobiles
	 */
	
	public int getNumberOfAutomobiles() {
		return this.automobiles.size();
    }
	
	/**
	 * Add a new response to the Question.
	 * 
	 * @precondition automobile != null
	 * 
	 * @param automobile the new automobile
	 * @return boolean
	 */
	public boolean addAutomobile(Automobile automobile) {
		if (automobile == null) {
			throw new IllegalArgumentException("automobile must not be null");
		}
		return this.automobiles.add(automobile);
	}
	
	/**
	 * Finds the passenger with the highest seat number
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the oldest automobile automobile if getNumberOfAutomobiles()
	 *         > 0 and Integer.MIN_VALUE if getNumberOfAutomobiles() == 0
	 */
	public int findOldestAutomobile() {
		if (this.automobiles.isEmpty()) {
			return Integer.MIN_VALUE;
		}
		Automobile oldestAutomobile = this.automobiles.get(0);
		for (Automobile currAutomobile : this.automobiles) {
			if (currAutomobile.getYear() < oldestAutomobile.getYear()) {
				oldestAutomobile = currAutomobile;
			}
		}
		return oldestAutomobile.getYear();
	}
	/**
	 * Finds the newest automobile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the newest automobile automobile if getNumberOfAutomobiles()
	 *         > 0 and Integer.MIN_VALUE if getNumberOfAutomobiles() == 0
	 */
	
	public int findNewestAutomobile() {
		if (this.automobiles.isEmpty()) {
			return Integer.MIN_VALUE;
		}
		Automobile newestAutomobile = this.automobiles.get(0);
		for (Automobile currAutomobile : this.automobiles) {
			if (currAutomobile.getYear() > newestAutomobile.getYear()) {
				newestAutomobile = currAutomobile;
			}
		}
		return newestAutomobile.getYear();
	}
	
	/**
	 * Finds the newest automobile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the newest automobile automobile if getNumberOfAutomobiles()
	 *         > 0 and Integer.MIN_VALUE if getNumberOfAutomobiles() == 0
	 * @param minYear the minimum year
	 * @param maxYear the maximum year
	 */
	public int findNumberOfSpecifiedAutomobiles(int minYear, int maxYear) {
		int count = 0;
		for (Automobile currAutomobile : this.automobiles) {
			if (currAutomobile.getYear() >= minYear && currAutomobile.getYear() <= maxYear) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Returns a String representation of the inventory
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a String representation of the inventory
	 */
	@Override
	public String toString() {
		return "Dealership: " + this.name + " " + "#Autos: " + this.automobiles.size();
	}
	
}
