package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAddAutomobile {

	@Test
	void testAddNullAutomobile() {
		Inventory inventory = new Inventory();

		assertThrows(IllegalArgumentException.class, ()->{inventory.addAutomobile(null);});
	}

	@Test
	void testAddAutomobileToInventoryWithNoAutomobiles() {
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		
		inventory.addAutomobile(automobile1);
		
		ArrayList<Automobile> automobiles = inventory.getAutomobiles();
		assertEquals(1, automobiles.size(), "checking number of automobiles");
		
		Automobile firstAutomobile = automobiles.get(0);
		assertEquals("Hyundai", firstAutomobile.getMake(), "checking make of first automobile");
		assertEquals("Sonata", firstAutomobile.getModel(), "checking model of first automobile");
		assertEquals(2012, firstAutomobile.getYear(), "checking year of first automobile");
		assertEquals(3000, firstAutomobile.getMiles(), "checking miles of first automobile");
		assertEquals(5000, firstAutomobile.getPrice(), "checking price of first automobile");
	}

	@Test
	void testAddAutomobileToInventoryWithMultipleAutomobiles() {
		//ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2020, 1000, 2000);
		//ACT
		inventory.addAutomobile(automobile2);
		//ASSERT
		ArrayList<Automobile> automobiles = inventory.getAutomobiles();
		assertEquals(2, automobiles.size(), "checking number of automobiles");
		
		Automobile firstAutomobile = automobiles.get(0);
		assertEquals("Hyundai", firstAutomobile.getMake(), "checking make of first automobile");
		assertEquals("Sonata", firstAutomobile.getModel(), "checking model of first automobile");
		assertEquals(2012, firstAutomobile.getYear(), "checking year of first automobile");
		assertEquals(3000, firstAutomobile.getMiles(), "checking miles of first automobile");
		assertEquals(5000, firstAutomobile.getPrice(), "checking price of first automobile");
		
		Automobile secondAutomobile = automobiles.get(1);
		assertEquals("Ford", secondAutomobile.getMake(), "checking make of second automobile");
		assertEquals("Mustang", secondAutomobile.getModel(), "checking model of second automobile");
		assertEquals(2020, secondAutomobile.getYear(), "checking year of second automobile");
		assertEquals(1000, secondAutomobile.getMiles(), "checking miles of second automobile");
		assertEquals(2000, secondAutomobile.getPrice(), "checking price of second automobile");
	}
	
	@Test
	void testAddAutomobileToInventoryWithOneAutomobile() {
		//ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2020, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2020, 10000, 6000);
		//ACT
		inventory.addAutomobile(automobile3);
		//ASSERT
		ArrayList<Automobile> automobiles = inventory.getAutomobiles();
		assertEquals(3, automobiles.size(), "checking number of automobiles");
		
		Automobile firstAutomobile = automobiles.get(0);
		assertEquals("Hyundai", firstAutomobile.getMake(), "checking make of first automobile");
		assertEquals("Sonata", firstAutomobile.getModel(), "checking model of first automobile");
		assertEquals(2012, firstAutomobile.getYear(), "checking year of first automobile");
		assertEquals(3000, firstAutomobile.getMiles(), "checking miles of first automobile");
		assertEquals(5000, firstAutomobile.getPrice(), "checking price of first automobile");
		
		Automobile secondAutomobile = automobiles.get(1);
		assertEquals("Ford", secondAutomobile.getMake(), "checking make of second automobile");
		assertEquals("Mustang", secondAutomobile.getModel(), "checking model of second automobile");
		assertEquals(2020, secondAutomobile.getYear(), "checking year of second automobile");
		assertEquals(1000, secondAutomobile.getMiles(), "checking miles of second automobile");
		assertEquals(2000, secondAutomobile.getPrice(), "checking price of second automobile");
		
		Automobile thirdAutomobile = automobiles.get(2);
		assertEquals("Toyota", thirdAutomobile.getMake(), "checking make of third automobile");
		assertEquals("Camry", thirdAutomobile.getModel(), "checking model of third automobile");
		assertEquals(2020, thirdAutomobile.getYear(), "checking year of third automobile");
		assertEquals(10000, thirdAutomobile.getMiles(), "checking miles of third automobile");
		assertEquals(6000, thirdAutomobile.getPrice(), "checking price of third automobile");
	}

}
