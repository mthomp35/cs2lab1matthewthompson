package edu.westga.cs1302.autodealer.test.automobile;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.test.TestingConstants;

class TestConstruction {

	@Test
	void testValidConstruction() {
		Automobile auto = new Automobile("Ford", "Focus", 2008, 108132, 6900.99);

		assertAll(() -> assertEquals("Ford", auto.getMake()), () -> assertEquals("Focus", auto.getModel()),
				() -> assertEquals(2008, auto.getYear()),
				() -> assertEquals(108132, auto.getMiles()),
				() -> assertEquals(6900.99, auto.getPrice(), TestingConstants.DELTA));

	}

	@Test
	void testNullMake() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile(null, "Focus", 2008, 10813, 6900.99));
	}

	@Test
	void testEmptyMake() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("", "Focus", 2008, 108132, 6900.99));
	}

	@Test
	void testNullModel() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("Ford", null, 2008, 108132, 6900.99));
	}

	@Test
	void testEmptyModel() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("Ford", "", 2008, 108132, 6900.99));
	}

	@Test
	void testYearJustBelowThreshold() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("Ford", "Focus", 1884, 108132, 6900.99));
	}

	@Test
	void testYearAtThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 1885, 108132, 6900.99);
		assertEquals(1885, auto.getYear());
	}

	@Test
	void testYearJustAboveThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 1886, 108132, 6900.99);
		assertEquals(1886, auto.getYear());
	}

	@Test
	void testMileageJustBelowThreshold() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("Ford", "Focus", 2008, -1, 6900.99));
	}

	@Test
	void testMileageAtThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 2008, 0, 6900.99);
		assertEquals(0, auto.getMiles());
	}

	@Test
	void testMileageJustAboveThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 2008, 1, 6900.99);
		assertEquals(1, auto.getMiles());
	}

	@Test
	void testPriceJustBelowThreshold() {
		assertThrows(IllegalArgumentException.class, () -> new Automobile("Ford", "Focus", 2008, 100, -0.01));
	}

	@Test
	void testPriceAtThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 2008, 100, 0);
		assertEquals(0, auto.getPrice(), TestingConstants.DELTA);
	}

	@Test
	void testPriceJustAboveThreshold() {
		Automobile auto = new Automobile("Ford", "Focus", 2008, 100, 0.01);
		assertEquals(0.01, auto.getPrice(), TestingConstants.DELTA);
	}

}
