package edu.westga.cs1302.autodealer.test;

/**
 * The Class TestingConstants.
 * 
 * @author CS1302
 */
public final class TestingConstants {
	public static final double DELTA = 0.000001;
}
