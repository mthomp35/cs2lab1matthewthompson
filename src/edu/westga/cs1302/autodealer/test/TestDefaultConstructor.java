package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Inventory;

class TestDefaultConstructor {

	@Test
	void testDefaultConstuctor() {
		// ARRANGE
		
		// ASSERT
		Inventory inventory = new Inventory();
		
		// ACT
		assertEquals(0, inventory.getAutomobiles().size(), "checking number of automobiles");
	}

}
