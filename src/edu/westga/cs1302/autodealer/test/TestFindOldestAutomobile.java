package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindOldestAutomobile {

	@Test
	void testEmptyCollection() {
		// ARRANGE
		Inventory inventory = new Inventory();

		// ACT
		int result = inventory.findOldestAutomobile();

		// ASSERT
		assertEquals(Integer.MIN_VALUE, result, "checking the oldest automobile");
	}

	@Test
	void testCollectionWithOneAutomobile() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		inventory.addAutomobile(automobile1);

		// ACT
		int result = inventory.findOldestAutomobile();

		// ASSERT
		assertEquals(2012, result, "checking the oldest automobile");
	}

	@Test
	void testCollectionWhereOldestIsFirstOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2020, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2020, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findOldestAutomobile();

		// ASSERT
		assertEquals(2012, result, "checking the oldest automobile");
	}
	@Test
	void testCollectionWhereOldestIsSecondOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2020, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2011, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2020, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findOldestAutomobile();

		// ASSERT
		assertEquals(2011, result, "checking the oldest automobile");
	}
	@Test
	void testCollectionWhereOldestIsThirdOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2020, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2020, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2009, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findOldestAutomobile();

		// ASSERT
		assertEquals(2009, result, "checking the oldest automobile");
	}

}
