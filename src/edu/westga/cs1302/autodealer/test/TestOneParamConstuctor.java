package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Inventory;

class TestOneParamConstuctor {

	@Test
	void testNullName() {
		// ARRANGE

		// ASSERT
		assertThrows(IllegalArgumentException.class, () -> {
			// ACT
			new Inventory(null);
		});
	}
	
	@Test
	void testValidName() {
		//ARRANGE
		
		//ACT
		Inventory inventory = new Inventory("Ford");
		//ASSERT
		assertEquals("Ford", inventory.getName(), "checking dealership name");
	}

}
