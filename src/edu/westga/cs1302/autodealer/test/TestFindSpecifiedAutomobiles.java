package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindSpecifiedAutomobiles {

	@Test
	void testEmptyCollection() {
		// ARRANGE
		Inventory inventory = new Inventory();

		// ACT
		int result = inventory.findNumberOfSpecifiedAutomobiles(2000, 2001);

		// ASSERT
		assertEquals(0, result, "checking the amount of specified automobiles");
	}

	@Test
	void testOneSpecifiedAutomobile() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2020, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2011, 1000, 2000);
		inventory.addAutomobile(automobile2);
		

		// ACT
		int result = inventory.findNumberOfSpecifiedAutomobiles(2000, 2011);

		// ASSERT
		assertEquals(1, result, "checking the amount of specified automobiles");
	}
	
	@Test
	void testMultipleSpecifiedAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2005, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2011, 1000, 2000);
		inventory.addAutomobile(automobile2);

		// ACT
		int result = inventory.findNumberOfSpecifiedAutomobiles(2000, 2011);


		// ASSERT
		assertEquals(2, result, "checking the amount of specified automobiles");
	}


}
