package edu.westga.cs1302.autodealer.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindNewestAutomobile {

	@Test
	void testEmptyCollection() {
		// ARRANGE
		Inventory inventory = new Inventory();

		// ACT
		int result = inventory.findNewestAutomobile();

		// ASSERT
		assertEquals(Integer.MIN_VALUE, result, "checking the newest automobile");
	}

	@Test
	void testCollectionWithOneAutomobile() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2012, 3000, 5000);
		inventory.addAutomobile(automobile1);

		// ACT
		int result = inventory.findNewestAutomobile();

		// ASSERT
		assertEquals(2012, result, "checking the Newest automobile");
	}

	@Test
	void testCollectionWhereNewestIsFirstOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2021, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2009, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2000, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findNewestAutomobile();

		// ASSERT
		assertEquals(2021, result, "checking the Newest automobile");
	}
	@Test
	void testCollectionWhereNewestIsSecondOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2000, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2006, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2001, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findNewestAutomobile();

		// ASSERT
		assertEquals(2006, result, "checking the Newest automobile");
	}
	@Test
	void testCollectionWhereNewestIsThirdOfThreeAutomobiles() {
		// ARRANGE
		Inventory inventory = new Inventory();
		Automobile automobile1 = new Automobile("Hyundai", "Sonata", 2009, 3000, 5000);
		inventory.addAutomobile(automobile1);
		Automobile automobile2 = new Automobile("Ford", "Mustang", 2000, 1000, 2000);
		inventory.addAutomobile(automobile2);
		Automobile automobile3 = new Automobile("Toyota", "Camry", 2010, 10000, 6000);
		inventory.addAutomobile(automobile3);
		// ACT
		int result = inventory.findNewestAutomobile();

		// ASSERT
		assertEquals(2010, result, "checking the Newest automobile");
	}
}
